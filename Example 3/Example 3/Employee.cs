﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example_3
{
    class Employee
    {
        private String _name;//always private
        private String _id;
        private Double _wage;
        private Int16 _personNum;

        public String Name
        {
            get { return _name; }
            set { _name = value; }
        }//with public getters and setters to allow for access

        public String ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public Double Wage
        {
            get { return _wage; }
            set { _wage = value; }
        }

        public Int16 PersonNum
        {
            get { return _personNum; }
            set { _personNum = value; }
        }

        public Employee()//default constructor
        {
        }

        public Employee(String Name, String ID, Double Wage, Int16 PersonNum)//overloaded constructor
        {
            this._name = Name;
            this._id = ID;
            this._wage = Wage;
            this._personNum = PersonNum;
        }
    }
}
