﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example_3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> Employees = new List<Employee>();//use a list instead of an array.  list takes the type of employee.cs
            Int32 numTimes = 0;

            Boolean correct = false;

            Employee p;

            String name = "";
            String ID = "";
            Double wage = 0;

            Console.WriteLine("How many employees do you want to enter?:");

            while (!correct)//a while for error checking
            {
                correct = true;//defaults to good
                try
                {
                    numTimes = Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    correct = false;//changes to not good if something goes wrong.
                    Console.WriteLine("Error: Please enter a number!");

                }
            }

            for (int x = 0; x < numTimes; x++)//input # of employees desired.
            {
                Console.Clear();
                Console.WriteLine("Person #{0}", x + 1);
                Console.Write("Please Enter employee name: ");
                name = Console.ReadLine();
                Console.Write("Please enter employee ID: ");
                ID = Console.ReadLine();
                Console.Write("Please enter employee wage: ");
                correct = false;
                while (!correct)
                {
                    correct = true;
                    try
                    {
                        wage = Convert.ToInt32(Console.ReadLine());
                    }
                    catch
                    {
                        correct = false;
                        Console.WriteLine("Error: Please enter a number!");

                    }
                }
                p = new Employee(name, ID, wage, Convert.ToInt16(x + 1));
                Employees.Add(p);
                
            }

            Console.WriteLine("Do you want to print out all people? (y/n)");

            if (Console.ReadLine().ToLower() == "y")//print all the things.
            {
                
                for(int x = 0; x < Employees.Count(); x++)
                {
                    Console.Clear();
                    Console.WriteLine("Person #{0}:", x + 1);
                    Console.WriteLine("Name:\t{0}", Employees[x].Name);
                    Console.WriteLine("Height:\t{0}", Employees[x].ID);
                    Console.WriteLine("Age\t{0}", Employees[x].Wage);
                    Console.Write("Press enter to continue");
                    String temp = Console.ReadLine();
                } 
            }
        }
    }
}
