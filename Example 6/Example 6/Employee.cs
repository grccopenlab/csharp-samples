﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example_6
{
    public class Employee : Person
    {
        private Double _wage;
        public Double Wage
        {
            get { return _wage; }
            set { _wage = value; }
        }

        public Employee()
        {
            //stuff
        }

        public Employee(String personType, String name, String idString, String returnString, Double wage)
            : base (personType, name, idString, returnString)
        {
            base.StrPersonType = "Employee";
            this._wage = wage;
        }
    }
}
