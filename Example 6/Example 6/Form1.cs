﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Example_6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddPerson ap = new AddPerson();

            if (rbCustomer.Checked)
            {
                ap.StrPersonType = "Customer";
            }
            else if (rbEmployee.Checked)
            {
                ap.StrPersonType = "Employee";
            }
            else if (rbSupplier.Checked)
            {
                ap.StrPersonType = "Supplier";
            }
            DialogResult dr = ap.ShowDialog();
            if (dr == DialogResult.OK)
            {
                lbPersonList.Enabled = true;
                btnDelete.Enabled = true;
                if(ap.StrPersonType == "Customer")
                {
                    Customer cust = new Customer(ap.StrPersonType, ap.StrName, ap.StrIdString, ap.StrReturnString, ap.StrEmail);
                    lbPersonList.Items.Add(cust);
                    lbPersonList.SelectedIndex = 0;
                }
                else if (ap.StrPersonType == "Employee")
                {
                    Employee emp = new Employee(ap.StrPersonType, ap.StrName, ap.StrIdString, ap.StrReturnString, ap.DblWage);
                    lbPersonList.Items.Add(emp);
                    lbPersonList.SelectedIndex = 0;
                }
                else if (ap.StrPersonType == "Supplier")
                {
                    Supplier sup = new Supplier(ap.StrPersonType, ap.StrName, ap.StrIdString, ap.StrReturnString, ap.StrProduct);
                    lbPersonList.Items.Add(sup);
                    lbPersonList.SelectedIndex = 0;
                }
                //lbPersonList.Items.Add(ap.GetReturnString);
            }
        }

        private void lbPersonList_DoubleClick(object sender, EventArgs e)//to edit the information, double click on entry
        {
            AddPerson ap;
            if(lbPersonList.SelectedItem.GetType() == typeof(Customer))//checks to see if instance is from the customer class
            {
                Customer cust;
                cust = lbPersonList.SelectedItem as Customer;//cast the instance from the listbox as a customer, into instance of customer.
                ap = new AddPerson(cust);
                DialogResult dr = ap.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    cust.StrPersonType = ap.StrPersonType;
                    cust.StrName = ap.StrName;
                    cust.StrIdString = ap.StrIdString;
                    cust.ReturnString = ap.StrReturnString;
                    cust.Email = ap.StrEmail;
                }
                lbPersonList.Items.Remove(cust);//update listbox
                lbPersonList.Items.Add(cust);
            }
            else if(lbPersonList.SelectedItem.GetType() == typeof(Employee))//or the employee class
            {
                Employee emp;
                emp = lbPersonList.SelectedItem as Employee;
                ap = new AddPerson(emp);
                DialogResult dr = ap.ShowDialog();
                if(dr == DialogResult.OK)
                {
                    emp.StrPersonType = ap.StrPersonType; ;
                    emp.StrName = ap.StrName;
                    emp.StrIdString = ap.StrIdString;
                    emp.ReturnString = ap.StrReturnString;
                    emp.Wage = ap.DblWage;
                }
                lbPersonList.Items.Remove(emp);
                lbPersonList.Items.Add(emp);
            }

            else if(lbPersonList.SelectedItem.GetType() == typeof(Supplier))//or the supplier class
            {
                Supplier sup;
                sup = lbPersonList.SelectedItem as Supplier;
                ap = new AddPerson(sup);
                DialogResult dr = ap.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    sup.StrPersonType = ap.StrPersonType;
                    sup.StrName = ap.StrName;
                    sup.StrIdString = ap.StrIdString;
                    sup.ReturnString = ap.StrReturnString;
                    sup.ProductType = ap.StrProduct;
                }
                lbPersonList.Items.Remove(sup);
                lbPersonList.Items.Add(sup);
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lbPersonList.Enabled = false;
            btnDelete.Enabled = false;
        }

        private void btnDelete_Click(object sender, EventArgs e)//removes entries from listbox
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to delete?", "delete", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                lbPersonList.Items.Remove(lbPersonList.SelectedItem);
            }

            if(lbPersonList.Items.Count < 1)
            {
                lbPersonList.Enabled = false;
                btnDelete.Enabled = false;
            }
        }
    }
}
