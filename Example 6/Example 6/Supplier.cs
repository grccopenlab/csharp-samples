﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example_6
{
    public class Supplier : Person
    {
        private String _productType;
        public String ProductType
        {
            get { return _productType; }
            set { _productType = value; }
        }

        public Supplier()
        {
            //stuff
        }

        public Supplier(String personType, String name, String idString, String returnString, String productType)
            : base(personType, name, idString, returnString)
        {
            base.StrPersonType = "Supplier";
            this._productType = productType;
        }
    }
}
