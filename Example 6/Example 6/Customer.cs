﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example_6
{
    public class Customer : Person
    {
        private String _email;
        public String Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public Customer()
        {
            //stuff
        }

        public Customer(String personType, String name, String idString, String returnString, String email)
            : base(personType, name, idString, returnString)
        {
            base.StrPersonType = "Customer";
            this._email = email;
        }
    }
}
