﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example_6
{
    public class Person
    {
        private String _personType;
        private String _name;
        private String _idString;
        private String _returnString;

        public String StrPersonType
        {
            get { return _personType; }
            set { _personType = value; }
        }
        public String StrName
        {
            get { return _name; }
            set { _name = value; }
        }
        public String StrIdString
        {
            get { return _idString; }
            set { _idString = value; }
        }
        public String ReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        public Person()
        {
            //stuff
        }

        public Person(String personType)
        {
            this._personType = personType;
        }

        public Person(String personType, String name, String idString, String returnString)
        {
            this._personType = personType;
            this._name = name;
            this._idString = idString;
            this._returnString = returnString;
        }

        public override string ToString()
        {
            return _returnString;
        }
    }
}
