﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Example_6
{
    public partial class AddPerson : Form
    {
        private String _personType;
        private String _name;
        private String _idString;
        private String _returnString;
        private String _changerString;
        private Double _wage;
        private String _email;
        private String _product;


        public String StrPersonType
        {
            get { return _personType; }
            set { _personType = value; }
        }
        public String StrName
        {
            get { return _name; }
            set { _name = value; }
        }
        public String StrIdString
        {
            get { return _idString; }
            set { _idString = value; }
        }
        public String StrReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }
        public String StrChangerString
        {
            get { return _changerString; }
            set { _changerString = value; }
        }
        public Double DblWage
        {
            get { return _wage; }
            set { _wage = value; }
        }
        public String StrEmail
        {
            get { return _email; }
            set { _email = value; }
        }
        public String StrProduct
        {
            get { return _product; }
            set { _product = value; }
        }

        public AddPerson()
        {
            InitializeComponent();
        }
        public AddPerson(Employee emp)//overloaded once
        {
            InitializeComponent();
            popForm(emp.StrPersonType, emp.StrName, emp.StrIdString, Convert.ToString(emp.Wage));
            
        }
        public AddPerson(Customer cust)//twice
        {
            InitializeComponent();
            popForm(cust.StrPersonType, cust.StrName, cust.StrIdString, cust.Email);
        }
        public AddPerson(Supplier sup)//three times
        {
            InitializeComponent();
            popForm(sup.StrPersonType, sup.StrName, sup.StrIdString, sup.ProductType);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Boolean isGood = true;
            String errorString = "";

            if(txtName.Text == "" || txtName.Text == " ")
            {
                errorString = "Name";
                isGood = false;
            }
            else
            {
                _name = txtName.Text;
            }

            if(txtId.Text == "" || txtId.Text == " ")
            {
                errorString = "ID";
                isGood = false;
            }
            else
            {
                _idString = txtId.Text;
            }

            if(txtChanger.Text == "" || txtChanger.Text == " ")
            {
                
                if (_personType == "Employee")
                {
                    errorString = "Wage";
                }
                else if (_personType == "Customer")
                {
                    errorString = "Email";
                }
                else if(_personType == "Supplier")
                {
                    errorString = "Product";
                }
                
                isGood = false;
            }

            if (_personType == "Employee")
            {
                try
                {
                    _wage = Convert.ToDouble(txtChanger.Text);
                }
                catch
                {
                    errorString = "Wage";
                    isGood = false;
                }
                
            }
            else if (_personType == "Customer")
            {
                _email = txtChanger.Text;
            }
            else if (_personType == "Supplier")
            {
                _product = txtChanger.Text;
            }

            if (isGood)
            {
                _returnString = string.Format("({0}) {1} - {2}", _personType, txtName.Text, txtId.Text);
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show(string.Format("Please enter a valid {0}", errorString), "Invalid entry");
            }
        }

        private void AddPerson_Load(object sender, EventArgs e)
        {
            if (lblPersonType.Text == "")
            {
                lblPersonType.Text = _personType;
            }
            if (_personType == "Employee")
            {
                lblChanger.Text = "Wage";
            }
            else if (_personType == "Customer")
            {
                lblChanger.Text = "Email";
            }
            else if (_personType == "Supplier")
            {
                lblChanger.Text = "Product";
            }
        }

        private void popForm(String PersonType, String Name, String ID, String Changer)//populate the form (function I made to do so, so this info didn't have to be duplicated
        {
            _personType = PersonType;
            _name = Name;
            _idString = ID;
            _changerString = Changer;

            lblPersonType.Text = _personType;
            txtName.Text = _name;
            txtId.Text = _idString;
            txtChanger.Text = _changerString;
        }
    }
}
