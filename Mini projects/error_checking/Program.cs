﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace error_checking
{
    class Program
    {
        static void Main(string[] args)
        {
            Boolean AllGood = false; //control variable for the while loop
            String PauseString = ""; //pause

            Int16 Num1 = 0;

            while (!AllGood)
            {
                AllGood = true;
                Console.Write("Enter a whole number: ");

                try//try to do whatever is in the brackets.
                {
                    Num1 = Convert.ToInt16(Console.ReadLine());
                }
                catch//if the try fails, catch the exception that is thrown so the user doesn't see it, and post your own
                {
                    AllGood = false; //set this to false so the while loops again
                    Console.WriteLine("Error: Incorrect entry!  Please try again");
                    Console.WriteLine("Press enter to continue!");
                    PauseString = Console.ReadLine();
                    Console.Clear();
                }
            }

            Console.WriteLine(String.Format("Your number is {0}", Num1));
            Console.WriteLine("Press enter to exit!");
            PauseString = Console.ReadLine();
        }
    }
}
