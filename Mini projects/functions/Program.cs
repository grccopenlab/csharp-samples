﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace functions
{
    class Program
    {
        static void Main(string[] args)
        {
            //change these numbers to see how the output changes
            Int16 Num1 = 7;
            Int16 Num2 = 3;

            Int32 wholeResult = 0;
            Double decimalResult = 0.0;

            Console.WriteLine("Adding number 1 and number 2:");
            wholeResult = AddNum(Num1, Num2); //AddNum takes 2 Ints and returns an int
            PrintOut(wholeResult); //PrintOut is overloaded, this instance takes 1 Int and returns nothing

            Console.WriteLine("Dividing number 1 and number 2:");
            decimalResult = DivideNum(Num1, Num2); //DivideNum takes 2 Ints and returns a Double
            PrintOut(decimalResult); //This instance takes 1 Double and returns nothing.

            Console.WriteLine("Press enter to quit program"); //pause
            String pauseString = Console.ReadLine();
        }

        static Int32 AddNum(Int16 Num1, Int16 Num2)
        {
            return (Num1 + Num2);

        }

        static Double DivideNum(Int16 Num1, Int16 Num2)
        {
            return (Convert.ToDouble(Num1) / Convert.ToDouble(Num2));
        }

        static void PrintOut(Int32 wholeResult)
        {
            Console.WriteLine(String.Format("Answer: {0}", wholeResult));
        }

        static void PrintOut(Double decimalResult)
        {
            Console.WriteLine(String.Format("Answer: {0}", decimalResult));
        }
    }
}
