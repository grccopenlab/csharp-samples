﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace input_output
{
    class Program
    {
        static void Main(string[] args)
        {
            String Name = ""; //declare a variable and initialize it

            Console.WriteLine("Hello world!"); //WriteLine returns at the end of the line so the next output is on a new line
            Console.WriteLine("What is your name?");

            Name = Console.ReadLine(); //readline intakes input in string form

            Console.WriteLine(String.Format("Hello {0}, Nice to meet you!", Name)); //string.format allows us to put variables into a string

            Console.Write("Press enter to continue!"); //a simple way to pause the screen.
            String PauseString = Console.ReadLine();

        }
    }
}
