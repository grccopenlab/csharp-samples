﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace loops
{
    class Program
    {
        static void Main(string[] args)
        {
            //control variable.  change the number to see how the loops react.
            Int16 Num1 = 7;

            //while loops only run if the condition is true
            Console.WriteLine("While loop 1:");
            while(Num1 > 0)
            {
                Console.WriteLine(String.Format("Num1 = {0}", Num1));
                Num1--; //decrements the variable by 1
            }

            Console.WriteLine("Press enter to continue");
            String pauseString = Console.ReadLine();
            Console.Clear(); //clears console

            //do while loops will always run once regardless of whether or not
            //the condition is true.
            Console.WriteLine("Do While loop:");
            do
            {
                Num1++;
                Console.WriteLine(String.Format("Num1 = {0}", Num1));
            } while (Num1 > 5);

            Console.WriteLine("Press enter to continue");
            pauseString = Console.ReadLine();
            Console.Clear();

            //second while loop to show counting up.
            Console.WriteLine("While loop 2:");
            while(Num1 < 7)
            {
                Console.WriteLine(String.Format("Num1 = {0}", Num1));
                Num1++; //increments the variable by 1
            }

            Console.WriteLine("Press enter to continue");
            pauseString = Console.ReadLine();
            Console.Clear();

            //for loop doesn't usually touch the control variable
            //it works with a second variable to count against the
            //control varaible.
            Console.WriteLine("For loop:");
            for (int x = 0; x < Num1; x++)
            {
                Console.WriteLine(String.Format("Num1 = {0}\tx = {1}", Num1, x));
            }
            
            Console.WriteLine("Press enter to quit program");
            pauseString = Console.ReadLine();
        }
    }
}
