﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace arrays_lists
{
    class Program
    {
        static void Main(string[] args)
        {
            String PauseString = "";//pause
            //defining the length of the aray seperate allows you to modify the size later on easier
            //if you have multiple arrays that coincide with each other, its easier to do it that way
            //and if you use a loop to fill the array with data, changing one value is easier than
            //changing 2+ values.
            Int16 ArrayLength = 5;
            Int16[] NumberArray = new Int16[ArrayLength]; //the array
            List<String> NameList = new List<string>(); //the list

            //change the numbers to see them change in the output//
            NumberArray[0] = 5;
            NumberArray[1] = 7;
            NumberArray[2] = 3;
            NumberArray[3] = 9;
            NumberArray[4] = 1;

            Console.WriteLine("Printing out the values in the NumberArray!");
            for(int x = 0; x < ArrayLength; x++)
            {
                Console.WriteLine(String.Format("Position {0}: NumberArray[{1}] {2}", x+1, x , NumberArray[x]));
            }

            //change the names to see them change in the output//
            NameList.Add("Bill");
            NameList.Add("Steve");
            NameList.Add("Benjamin");
            NameList.Add("Erik");
            NameList.Add("Olivia");

            //pause
            Console.Write("Press enter to continue!");
            PauseString = Console.ReadLine();
            Console.Clear();

            Console.WriteLine("Printing out the values in the NameList!");
            for (int x = 0; x < NameList.Count; x++)
            {
                Console.WriteLine(String.Format("Position {0}: NameList[{1}] {2}", x + 1, x, NameList[x]));
            }

            NameList.Sort(); //sorts the list (one of the functions that makes lists superior to arrays)
            Console.WriteLine("-------------------------------");
            Console.WriteLine("Printing out the values in the NameList after sort!");
            for (int x = 0; x < NameList.Count; x++)
            {
                Console.WriteLine(String.Format("Position {0}: NameList[{1}] {2}", x + 1, x, NameList[x]));
            }

            Console.Write("Press enter to continue!"); //a simple way to pause the screen.
            PauseString = Console.ReadLine();
        }
    }
}
