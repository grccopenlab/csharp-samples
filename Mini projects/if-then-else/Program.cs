﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace if_then_else
{
    class Program
    {
        static void Main(string[] args)
        {
            //change the numbers to see how the if statement reacts
            Int16 Num1 = 5;
            Int16 Num2 = 13;

            if(Num1 < Num2)
            {
                Console.WriteLine("Number 1 is larger");
            }
            else if(Num1 > Num2)
            {
                Console.WriteLine("Number 2 is larger");
            }
            else
            {
                Console.WriteLine("Both numbers are equal");
            }

            Console.WriteLine("Press enter to quit program");
            String pauseString = Console.ReadLine();
        }
    }
}
