﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Example_5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddPerson ap = new AddPerson(); //instance of secondary form
            if (rbCustomer.Checked)
            {
                ap.StrPersonType = "Customer";
            }
            else if (rbEmployee.Checked)
            {
                ap.StrPersonType = "Employee";
            }
            else if(rbSupplier.Checked)
            {
                ap.StrPersonType = "Supplier";
            }
            DialogResult dr = ap.ShowDialog();//this is good for error checking
            if(dr == DialogResult.OK)//if ok was clicked in addperson then run this
            {
                lbPersonList.Items.Add(ap.StrReturnString);//getting return string from addperson form to put into listbox
            }
        }
    }
}
