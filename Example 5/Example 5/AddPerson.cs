﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Example_5
{
    public partial class AddPerson : Form
    {
        private String _personType;
        private String _name;
        private String _idString;
        private String _returnString;

        public String StrPersonType
        {
            get { return _personType; }
            set { _personType = value; }
        }
        public String StrName
        {
            get { return _name; }
            set { _name = value; }
        }
        public String StrIdString
        {
            get { return _idString; }
            set { _idString = value; }
        }
        public String StrReturnString
        {
            get { return _returnString; }
            set { _returnString = value; }
        }

        public AddPerson()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)//if addbutton clicked then do these things
        {
            if (txtName.Text == "" || txtId.Text == "")
            {
                MessageBox.Show("Please enter a name and ID", "Invalid entry");
            }
            else
            {
                _returnString = string.Format("({0}) {1} - {2}", _personType, txtName.Text, txtId.Text);
                this.DialogResult = DialogResult.OK;
            }

        }

        private void AddPerson_Load(object sender, EventArgs e) //set the label with what type of person it is when this form loads.
        {
            lblPersonType.Text = _personType;
            
        }
    }
}
