﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Example_4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(txtName.Text == "")//grab from textbox
            {
                MessageBox.Show("Please enter a name", "Invalid entry");
            }
            else
            {
                if(rbEmployee.Checked)//radio button
                {
                    addPerson("Employee");
                }
                else if (rbCustomer.Checked)
                {
                    addPerson("Customer");
                }
                else if(rbSupplier.Checked)
                {
                    addPerson("Supplier");
                }
            }
        }

        private void addPerson(string type)//function made to add people to listbox!
        {
            lbPersonList.Items.Add(string.Format("{0} - {1}", txtName.Text, type));
        }

    }
}