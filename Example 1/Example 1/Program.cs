﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace example_1
{
    class Program
    {
        static void Main(string[] args)
        {
            String empName;
            String empID;
            String empWage;

            Console.Write("Please enter an employee name: "); //output
            empName = Console.ReadLine();//input into string

            Console.Write(String.Format("Please enter {0}'s employee ID: ", empName));
            empID = Console.ReadLine();

            Console.Write(String.Format("Please enter {0}'s wage: ", empName));
            empWage = Console.ReadLine();

            Console.WriteLine(String.Format("Employee Name:\t{0}", empName));//output information from variables
            Console.WriteLine(String.Format("Employee ID:\t{0}", empID));
            Console.WriteLine(String.Format("Employee wage:\t{0}", empWage));
            Console.WriteLine("Hit enter to close the program!");
            String temp = Console.ReadLine();
        }
    }
}