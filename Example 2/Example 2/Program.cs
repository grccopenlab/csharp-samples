﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Int32 counter;

            String[] empName;//3 arrays
            String[] empID;
            Double[] empWage;


            Console.Write("How many employees do you want to enter?: ");
            counter = Convert.ToInt32(Console.ReadLine());//convert intput into integer

            empName = new String[counter];//define how big they are with user input
            empID = new String[counter];
            empWage = new Double[counter];

            for(int x = 0; x < counter; x++)//for loop to allow entry into arrays defined by user input
            {
                Console.Clear();//erase screen
                Console.WriteLine(String.Format("Please enter the information for employee #{0}:", (x + 1)));
                Console.Write("Name: ");
                empName[x] = Console.ReadLine();//inputs into the x spot of the array

                Console.Write("ID: ");
                empID[x] = Console.ReadLine();

                Console.Write("Wage: ");
                empWage[x] = Convert.ToDouble(Console.ReadLine());
            }

            Console.WriteLine("Would you like to print out your employees?");

            if (Console.ReadLine().ToLower() == "y")//if user hits y or Y this will work.
            {
                for(int x = 0; x < counter; x++)//output all the things.
                {
                    Console.Clear();
                    Console.WriteLine(String.Format("Employee #{0}:", (x + 1)));
                    Console.WriteLine(String.Format("Name:\t{0}", empName[x]));
                    Console.WriteLine(String.Format("ID:\t{0}", empID[x]));
                    Console.WriteLine(String.Format("Wage:\t{0}", Convert.ToString(empWage[x])));
                    Console.WriteLine("Press Enter to Continue!");
                    String Temp = Console.ReadLine();
                }
            }
        }
    }
}
